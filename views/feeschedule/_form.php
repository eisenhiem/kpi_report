<?php

use app\models\FeeTemplate;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

$fs = ArrayHelper::map(FeeTemplate::find()->all(), 'fee_schedule_id', 'fee_schedule_name');

/* @var $this yii\web\View */
/* @var $model app\models\FeeSchedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fee-schedule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'report_date')->widget(DatePicker::ClassName(),
    [
        'name' => 'report_date', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่รายงาน'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>


    <?= $form->field($model, 'fee_schedule_id')->dropDownList($fs, ['prompt'=>'เลือกข้อ Fee Schedule']) ?>

    <?= $form->field($model, 'target')->textInput() ?>

    <?= $form->field($model, 'result')->textInput() ?>

    <?= $form->field($model, 'result_total')->textInput() ?>

    <?= $form->field($model, 'rep')->textInput() ?>

    <?= $form->field($model, 'deny')->textInput() ?>

    <?= $form->field($model, 'rep_total')->textInput() ?>

    <?= $form->field($model, 'rec')->textInput() ?>

    <?php // = $form->field($model, 'd_update')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
