<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FeeSchedule */

$this->title = 'Update Fee Schedule: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fee Schedules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fee-schedule-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
