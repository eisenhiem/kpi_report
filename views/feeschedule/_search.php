<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeeScheduleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fee-schedule-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'report_date') ?>

    <?= $form->field($model, 'fee_schedule_id') ?>

    <?= $form->field($model, 'target') ?>

    <?= $form->field($model, 'result') ?>

    <?php // echo $form->field($model, 'result_total') ?>

    <?php // echo $form->field($model, 'rep') ?>

    <?php // echo $form->field($model, 'deny') ?>

    <?php // echo $form->field($model, 'rep_total') ?>

    <?php // echo $form->field($model, 'rec') ?>

    <?php // echo $form->field($model, 'd_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
