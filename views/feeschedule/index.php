<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FeeScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายงาน Fee Schedules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fee-schedule-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('บันทึก Fee Schedule', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'report_date',
            'fee_schedule_id',
            'target',
            'result',
            'result_total',
            'rep',
            'deny',
            'rep_total',
            'rec',
            //'d_update',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
