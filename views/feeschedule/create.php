<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FeeSchedule */

$this->title = 'Create Fee Schedule';
$this->params['breadcrumbs'][] = ['label' => 'Fee Schedules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fee-schedule-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
