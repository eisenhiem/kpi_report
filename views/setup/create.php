<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SetupIndex */

$this->title = 'Create Setup Index';
$this->params['breadcrumbs'][] = ['label' => 'Setup Indices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setup-index-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
