<?php

use app\models\SetupIndex;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SetupIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="setup-index-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'graph1')->radioList([ 'line' => 'Line', 'column' => 'Column', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'graph2')->radioList([ 'line' => 'Line', 'column' => 'Column', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'graph3')->radioList([ 'line' => 'Line', 'column' => 'Column', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'graph4')->radioList([ 'line' => 'Line', 'column' => 'Column', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'columns_show')->checkboxList(SetupIndex::getItems()) ?>

    <?= $form->field($model, 'card1')->radioList(SetupIndex::getItems()) ?>

    <?= $form->field($model, 'card2')->radioList(SetupIndex::getItems()) ?>

    <?= $form->field($model, 'card3')->radioList(SetupIndex::getItems()) ?>

    <?= $form->field($model, 'card4')->radioList(SetupIndex::getItems()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
