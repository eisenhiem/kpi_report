<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SetupIndexSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Setup Indices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setup-index-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Setup Index', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'graph1',
            'graph2',
            'graph3',
            'graph4',
            //'columns_show',
            //'card1',
            //'card2',
            //'card3',
            //'card4',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
