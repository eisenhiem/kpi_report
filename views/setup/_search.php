<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SetupIndexSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="setup-index-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'graph1') ?>

    <?= $form->field($model, 'graph2') ?>

    <?= $form->field($model, 'graph3') ?>

    <?= $form->field($model, 'graph4') ?>

    <?php // echo $form->field($model, 'columns_show') ?>

    <?php // echo $form->field($model, 'card1') ?>

    <?php // echo $form->field($model, 'card2') ?>

    <?php // echo $form->field($model, 'card3') ?>

    <?php // echo $form->field($model, 'card4') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
