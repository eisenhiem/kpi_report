<?php

use yii\helpers\Html;
use yii\grid\GridView;
use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\SeriesDataHelper;

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    'yearbudget',
    'op_visit',
    //'op_hn',
    //'op_per_day',
    'op_refer',
    //'op_death',
    //'op_accdent',
    //'op_head_injury',
    //'op_stroke',
    //'op_stemi',
    'ip_admit',
    //'ip_dc',
    'ip_refer',
    //'ip_losd',
    //'ip_bed_rate',
    'ip_sum_adjrw',
    'ip_cmi',
    //'op_uc',
    //'op_sss',
    //'op_lgo',
    //'op_ofc',
    //'op_prb',
    //'op_other',
    //'ip_uc',
    //'ip_sss',
    //'ip_lgo',
    //'ip_ofc',
    //'ip_other',
    //'d_update',

];
/* @var $this yii\web\View */
/* @var $searchModel app\models\SummaryReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายงานสรุป';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="summary-report-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <!-- แสดงกราฟ !-->
    <div class="col-md-6">
    <div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="glyphicon glyphicon-signal"></i>
            จำนวนผู้ป่วยนอกแยกรายปีงบประมาณ</h3>
    </div>
    <div class="panel-body">
        <?php
        echo Highcharts::widget([
            'options' => [
                'title' => ['text' => 'จำนวนผู้ป่วยนอกแยกรายงบประมาณ'],
                'xAxis' => [
                    'categories' => new SeriesDataHelper($dataProvider, ['yearbudget']),
                ],
                'yAxis' => [
                    'title' => ['text' => 'จำนวน']
                ],
                'series' => [
                    [
                        'type' => 'line',
                        'name' => 'จำนวนผู้มารับบริการ(ครั้ง)',
                        'data' => new SeriesDataHelper($dataProvider, ['op_visit']),
                    ],
                    [
                        'type' => 'line',
                        'name' => 'จำนวนผู้มารับบริการ(คน)',
                        'data' => new SeriesDataHelper($dataProvider, ['op_hn']),
                    ],
                ]
            ]
        ]);
        ?>
    </div>
    </div>
    </div>
    <!-- กราฟ 2 !-->
    <div class="col-md-6">
    <div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="glyphicon glyphicon-signal"></i>
            จำนวนผู้ป่วยในแยกรายปีงบประมาณ</h3>
    </div>
    <div class="panel-body">
        <?php
        echo Highcharts::widget([
            'options' => [
                'title' => ['text' => 'จำนวนผู้ป่วยในแยกรายงบประมาณ'],
                'xAxis' => [
                    'categories' => new SeriesDataHelper($dataProvider, ['yearbudget']),
                ],
                'yAxis' => [
                    'title' => ['text' => 'จำนวน(admit)']
                ],
                'series' => [
                    [
                        'type' => 'line',
                        'name' => 'จำนวนผู้ที่รับรักษาผู้ป่วยใน',
                        'data' => new SeriesDataHelper($dataProvider, ['ip_admit']),
                    ],
                ]
            ]
        ]);
        ?>
    </div>
    </div>
    </div>
    <!-- กราฟ 3 !-->
    <div class="col-md-6">
    <div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="glyphicon glyphicon-signal"></i>
            ค่า CMI แยกรายปีงบประมาณ</h3>
    </div>
    <div class="panel-body">
        <?php
        echo Highcharts::widget([
            'options' => [
                'title' => ['text' => 'ค่า CMI แยกรายงบประมาณ'],
                'xAxis' => [
                    'categories' => new SeriesDataHelper($dataProvider, ['yearbudget']),
                ],
                'yAxis' => [
                    'title' => ['text' => 'CMI']
                ],
                'series' => [
                    [
                        'type' => 'line',
                        'name' => 'CMI',
                        'data' => new SeriesDataHelper($dataProvider, ['ip_cmi']),
                    ],
                ]
            ]
        ]);
        ?>
    </div>
    </div>
    </div>
    <!-- กราฟ 4 !-->
    <div class="col-md-6">
    <div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="glyphicon glyphicon-signal"></i>
            อัตราการครองเตียงแยกรายปีงบประมาณ</h3>
    </div>
    <div class="panel-body">
        <?php
        echo Highcharts::widget([
            'options' => [
                'title' => ['text' => 'อัตราการครองเตียงแยกรายงบประมาณ'],
                'xAxis' => [
                    'categories' => new SeriesDataHelper($dataProvider, ['yearbudget']),
                ],
                'yAxis' => [
                    'title' => ['text' => 'ร้อยละ']
                ],
                'series' => [
                    [
                        'type' => 'line',
                        'name' => 'อัตราการครองเตียง',
                        'data' => new SeriesDataHelper($dataProvider, ['ip_bed_rate']),
                    ],
                ]
            ]
        ]);
        ?>
    </div>
    </div>
    </div>
    <!-- กราฟ 5 !-->
    <div class="col-md-6">
    <div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="glyphicon glyphicon-signal"></i>
            จำนวนการส่งต่อผู้ป่วยนอกแยกรายปีงบประมาณ</h3>
    </div>
    <div class="panel-body">
        <?php
        echo Highcharts::widget([
            'options' => [
                'title' => ['text' => 'จำนวนการส่งต่อผู้ป่วยนอกแยกรายปีงบประมาณ'],
                'xAxis' => [
                    'categories' => new SeriesDataHelper($dataProvider, ['yearbudget']),
                ],
                'yAxis' => [
                    'title' => ['text' => 'จำนวน']
                ],
                'series' => [
                    [
                        'type' => 'line',
                        'name' => 'ส่งต่อผู้ป่วยนอก',
                        'data' => new SeriesDataHelper($dataProvider, ['op_refer']),
                    ],
                ]
            ]
        ]);
        ?>
    </div>
    </div>
    </div>
    <!-- กราฟ 6 !-->
    <div class="col-md-6">
    <div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="glyphicon glyphicon-signal"></i>
            จำนวนการส่งต่อผู้ป่วยในแยกรายปีงบประมาณ</h3>
    </div>
    <div class="panel-body">
        <?php
        echo Highcharts::widget([
            'options' => [
                'title' => ['text' => 'จำนวนการส่งต่อผู้ป่วยในแยกรายปีงบประมาณ'],
                'xAxis' => [
                    'categories' => new SeriesDataHelper($dataProvider, ['yearbudget']),
                ],
                'yAxis' => [
                    'title' => ['text' => 'จำนวน']
                ],
                'series' => [
                    [
                        'type' => 'line',
                        'name' => 'ส่งต่อผู้ป่วยใน',
                        'data' => new SeriesDataHelper($dataProvider, ['ip_refer']),
                    ],
                ]
            ]
        ]);
        ?>
    </div>
    </div>
    </div>
    <!-- แสดงตาราง !-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => $columns,

    ]); ?>


</div>
