<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SummaryReport */

$this->title = 'Update Summary Report: ' . $model->yearbudget;
$this->params['breadcrumbs'][] = ['label' => 'Summary Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->yearbudget, 'url' => ['view', 'id' => $model->yearbudget]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="summary-report-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
