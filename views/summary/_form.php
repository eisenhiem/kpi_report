<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SummaryReport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="summary-report-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'yearbudget')->textInput() ?>

    <?= $form->field($model, 'op_visit')->textInput() ?>

    <?= $form->field($model, 'op_hn')->textInput() ?>

    <?= $form->field($model, 'op_per_day')->textInput() ?>

    <?= $form->field($model, 'op_refer')->textInput() ?>

    <?= $form->field($model, 'op_death')->textInput() ?>

    <?= $form->field($model, 'op_accdent')->textInput() ?>

    <?= $form->field($model, 'op_head_injury')->textInput() ?>

    <?= $form->field($model, 'op_stroke')->textInput() ?>

    <?= $form->field($model, 'op_stemi')->textInput() ?>

    <?= $form->field($model, 'ip_admit')->textInput() ?>

    <?= $form->field($model, 'ip_refer')->textInput() ?>

    <?= $form->field($model, 'ip_losd')->textInput() ?>

    <?= $form->field($model, 'ip_bed_rate')->textInput() ?>

    <?= $form->field($model, 'ip_sum_adjrw')->textInput() ?>

    <?= $form->field($model, 'ip_cmi')->textInput() ?>

    <?= $form->field($model, 'op_uc')->textInput() ?>

    <?= $form->field($model, 'op_sss')->textInput() ?>

    <?= $form->field($model, 'op_lgo')->textInput() ?>

    <?= $form->field($model, 'op_ofc')->textInput() ?>

    <?= $form->field($model, 'op_other')->textInput() ?>

    <?= $form->field($model, 'ip_uc')->textInput() ?>

    <?= $form->field($model, 'ip_sss')->textInput() ?>

    <?= $form->field($model, 'ip_lgo')->textInput() ?>

    <?= $form->field($model, 'ip_ofc')->textInput() ?>

    <?= $form->field($model, 'ip_other')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
