<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SummaryReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายงานสรุป';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="summary-report-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มรายงานสรุป', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'summary_id',
            'yearbudget',
            'op_visit',
            //'op_hn',
            //'op_per_day',
            'op_refer',
            //'op_death',
            //'op_accdent',
            //'op_head_injury',
            //'op_stroke',
            //'op_stemi',
            'ip_admit',
            //'ip_dc',
            'ip_refer',
            //'ip_losd',
            //'ip_bed_rate',
            'ip_sum_adjrw',
            'ip_cmi',
            //'op_uc',
            //'op_sss',
            //'op_lgo',
            //'op_ofc',
            //'op_prb',
            //'op_other',
            //'ip_uc',
            //'ip_sss',
            //'ip_lgo',
            //'ip_ofc',
            //'ip_other',
            //'d_update',

            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-danger'],
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                      return Html::a('แก้ไข',['summary/update','id'=>$model->yearbudget],['class' => 'btn btn-warning']);
                    }
                ]
             ],
        ],

    ]); ?>


</div>
