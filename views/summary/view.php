<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SummaryReport */

$this->title = $model->yearbudget;
$this->params['breadcrumbs'][] = ['label' => 'Summary Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="summary-report-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->yearbudget], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->yearbudget], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'yearbudget',
            'op_visit',
            'op_hn',
            'op_per_day',
            'op_refer',
            'op_death',
            'op_accdent',
            'op_head_injury',
            'op_stroke',
            'op_stemi',
            'ip_admit',
            'ip_refer',
            'ip_losd',
            'ip_bed_rate',
            'ip_sum_adjrw',
            'ip_cmi',
            'op_uc',
            'op_sss',
            'op_lgo',
            'op_ofc',
            'op_other',
            'ip_uc',
            'ip_sss',
            'ip_lgo',
            'ip_ofc',
            'ip_other',
            'd_update',
        ],
    ]) ?>

</div>
