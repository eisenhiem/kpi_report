<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SummaryReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="summary-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'summary_id') ?>

    <?= $form->field($model, 'yearbudget') ?>

    <?= $form->field($model, 'op_visit') ?>

    <?= $form->field($model, 'op_hn') ?>

    <?= $form->field($model, 'op_per_day') ?>

    <?php // echo $form->field($model, 'op_refer') ?>

    <?php // echo $form->field($model, 'op_death') ?>

    <?php // echo $form->field($model, 'op_accdent') ?>

    <?php // echo $form->field($model, 'op_head_injury') ?>

    <?php // echo $form->field($model, 'op_stroke') ?>

    <?php // echo $form->field($model, 'op_stemi') ?>

    <?php // echo $form->field($model, 'ip_admit') ?>

    <?php // echo $form->field($model, 'ip_dc') ?>

    <?php // echo $form->field($model, 'ip_refer') ?>

    <?php // echo $form->field($model, 'ip_losd') ?>

    <?php // echo $form->field($model, 'ip_bed_rate') ?>

    <?php // echo $form->field($model, 'ip_sum_adjrw') ?>

    <?php // echo $form->field($model, 'ip_cmi') ?>

    <?php // echo $form->field($model, 'op_uc') ?>

    <?php // echo $form->field($model, 'op_sss') ?>

    <?php // echo $form->field($model, 'op_lgo') ?>

    <?php // echo $form->field($model, 'op_ofc') ?>

    <?php // echo $form->field($model, 'op_prb') ?>

    <?php // echo $form->field($model, 'op_other') ?>

    <?php // echo $form->field($model, 'ip_uc') ?>

    <?php // echo $form->field($model, 'ip_sss') ?>

    <?php // echo $form->field($model, 'ip_lgo') ?>

    <?php // echo $form->field($model, 'ip_ofc') ?>

    <?php // echo $form->field($model, 'ip_other') ?>

    <?php // echo $form->field($model, 'd_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
