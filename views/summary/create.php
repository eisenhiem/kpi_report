<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SummaryReport */

$this->title = 'Create Summary Report';
$this->params['breadcrumbs'][] = ['label' => 'Summary Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="summary-report-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
