<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kpi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kpi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kpi_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kpi_template')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'kpi_target')->textInput() ?>

    <?= $form->field($model, 'kpi_operate')->dropDownList([ '=' => '=', '>=' => '>=', '<=' => '<=', '>' => '>', '<' => '<', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'kpi_owner')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kpi_from')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
