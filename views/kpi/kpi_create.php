<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kpi */

$this->title = 'เพิ่ม KPI';
$this->params['breadcrumbs'][] = ['label' => 'Kpis', 'url' => ['kpiindex']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_kpi_form', [
        'model' => $model,
    ]) ?>

</div>
