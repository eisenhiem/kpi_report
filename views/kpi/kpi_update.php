<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kpi */

$this->title = 'แก้ไข KPI: ' . $model->kpi_id;
$this->params['breadcrumbs'][] = ['label' => 'Kpis', 'url' => ['kpiindex']];
$this->params['breadcrumbs'][] = ['label' => $model->kpi_id, 'url' => ['kpiview', 'id' => $model->kpi_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kpi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_kpi_form', [
        'model' => $model,
    ]) ?>

</div>
