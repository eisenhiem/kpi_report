<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FeeTemplate */

$this->title = 'เพิ่ม Fee Schedule Template';
$this->params['breadcrumbs'][] = ['label' => 'Fee Templates', 'url' => ['fsindex']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fee-template-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_fs_form', [
        'model' => $model,
    ]) ?>

</div>
