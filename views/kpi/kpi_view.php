<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kpi */

$this->title = $model->kpi_id;
$this->params['breadcrumbs'][] = ['label' => 'Kpis', 'url' => ['kpiindex']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="kpi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['kpiupdate', 'id' => $model->kpi_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['kpidelete', 'id' => $model->kpi_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kpi_id',
            'kpi_name',
            'kpi_template:ntext',
            'kpi_target',
            'kpi_operate',
            'kpi_owner',
            'kpi_from',
        ],
    ]) ?>

</div>
