<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FeeTemplate */

$this->title = 'Update Fee Template: ' . $model->fee_schedule_id;
$this->params['breadcrumbs'][] = ['label' => 'Fee Templates', 'url' => ['fsindex']];
$this->params['breadcrumbs'][] = ['label' => $model->fee_schedule_id, 'url' => ['fsview', 'id' => $model->fee_schedule_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fee-template-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_fs_form', [
        'model' => $model,
    ]) ?>

</div>
