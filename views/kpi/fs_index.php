<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FeeTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fee Schedule Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fee-template-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่ม Fee Schedule Template', ['fscreate'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_fs_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'fee_schedule_id',
            'fee_schedule_name',
            'template:ntext',
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-danger'],
                'template'=>'{edit}',
                'buttons'=>[
                    'edit' => function($url,$model,$key){
                      return Html::a('แก้ไข',['kpi/fsupdate','id'=>$model->fee_schedule_id],['class' => 'btn btn-warning']);
                    }
                ]
             ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
