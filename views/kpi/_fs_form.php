<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeeTemplate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fee-template-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fee_schedule_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'template')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
