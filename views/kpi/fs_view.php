<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FeeTemplate */

$this->title = $model->fee_schedule_id;
$this->params['breadcrumbs'][] = ['label' => 'Fee Templates', 'url' => ['fsindex']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="fee-template-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['fsupdate', 'id' => $model->fee_schedule_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['fsdelete', 'id' => $model->fee_schedule_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fee_schedule_id',
            'fee_schedule_name',
            'template:ntext',
        ],
    ]) ?>

</div>
