<?php

use app\models\Kpi;

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

$kpi = ArrayHelper::map(Kpi::find()->all(), 'kpi_id', 'kpi_name');

/* @var $this yii\web\View */
/* @var $model app\models\Report */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'yearbudget')->textInput() ?>

    <?= $form->field($model, 'report_date')->widget(DatePicker::ClassName(),
    [
        'name' => 'report_date', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่รายงาน'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'kpi_id')->dropDownList($kpi, ['prompt'=>'เลือกข้อ KPI']) ?>

    <?= $form->field($model, 'report_target')->textInput() ?>

    <?= $form->field($model, 'report_result')->textInput() ?>

    <?= $form->field($model, 'report_rate')->textInput() ?>

    <?//= $form->field($model, 'd_update')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
