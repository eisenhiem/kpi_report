<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php //= $form->field($model, 'report_id') ?>

    <?= $form->field($model, 'yearbudget') ?>

    <?php //= $form->field($model, 'report_date') ?>

    <?php //= $form->field($model, 'kpi_id') ?>

    <?php //= $form->field($model, 'report_target') ?>

    <?php // echo $form->field($model, 'report_result') ?>

    <?php // echo $form->field($model, 'report_rate') ?>

    <?php // echo $form->field($model, 'd_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
