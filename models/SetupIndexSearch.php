<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SetupIndex;

/**
 * SetupIndexSearch represents the model behind the search form of `app\models\SetupIndex`.
 */
class SetupIndexSearch extends SetupIndex
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['graph1', 'graph2', 'graph3', 'graph4', 'columns_show', 'card1', 'card2', 'card3', 'card4'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SetupIndex::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'graph1', $this->graph1])
            ->andFilterWhere(['like', 'graph2', $this->graph2])
            ->andFilterWhere(['like', 'graph3', $this->graph3])
            ->andFilterWhere(['like', 'graph4', $this->graph4])
            ->andFilterWhere(['like', 'columns_show', $this->columns_show])
            ->andFilterWhere(['like', 'card1', $this->card1])
            ->andFilterWhere(['like', 'card2', $this->card2])
            ->andFilterWhere(['like', 'card3', $this->card3])
            ->andFilterWhere(['like', 'card4', $this->card4]);

        return $dataProvider;
    }
}
