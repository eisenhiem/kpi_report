<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kpi".
 *
 * @property int $kpi_id
 * @property string|null $kpi_name
 * @property string|null $kpi_template
 * @property float|null $kpi_target
 * @property string|null $kpi_operate
 * @property string|null $kpi_owner
 * @property string|null $kpi_from
 */
class Kpi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kpi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kpi_template', 'kpi_operate'], 'string'],
            [['kpi_target'], 'number'],
            [['kpi_name', 'kpi_owner', 'kpi_from'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kpi_id' => 'Kpi ID',
            'kpi_name' => 'รายการ KPI',
            'kpi_template' => 'Kpi Template',
            'kpi_target' => 'เป้าหมาย',
            'kpi_operate' => 'เครื่องหมาย',
            'kpi_owner' => 'ผู้รับผิดชอบ KPI',
            'kpi_from' => 'ที่มาของ KPI',
        ];
    }
}
