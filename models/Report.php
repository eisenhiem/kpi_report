<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report".
 *
 * @property int $report_id
 * @property int|null $yearbudget
 * @property string|null $report_date
 * @property int|null $kpi_id
 * @property float|null $report_target
 * @property float|null $report_result
 * @property float|null $report_rate
 * @property string|null $d_update
 */
class Report extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearbudget', 'kpi_id'], 'integer'],
            [['report_date', 'd_update'], 'safe'],
            [['report_target', 'report_result', 'report_rate'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'report_id' => 'Report ID',
            'yearbudget' => 'ปีงบประมาณ',
            'report_date' => 'วันที่รายงาน',
            'kpi_id' => 'Kpi ID',
            'report_target' => 'เป้าหมาย',
            'report_result' => 'ผลงาน',
            'report_rate' => 'ร้อยละ',
            'd_update' => 'วันที่ปรับปรุงข้อมูล',
        ];
    }

    public function getKpi(){
        return $this->hasOne(Kpi::className(),['kpi_id' => 'kpi_id']);
    }

}
