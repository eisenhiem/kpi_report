<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "op_top_dx".
 *
 * @property int $yearbudget
 * @property string|null $op_top_dx_1
 * @property string|null $op_top_dx_2
 * @property string|null $op_top_dx_3
 * @property string|null $op_top_dx_4
 * @property string|null $op_top_dx_5
 * @property string|null $d_update
 */
class OpTopDx extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'op_top_dx';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearbudget'], 'required'],
            [['yearbudget'], 'integer'],
            [['d_update'], 'safe'],
            [['op_top_dx_1', 'op_top_dx_2', 'op_top_dx_3', 'op_top_dx_4', 'op_top_dx_5'], 'string', 'max' => 255],
            [['yearbudget'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'yearbudget' => 'Yearbudget',
            'op_top_dx_1' => 'Op Top Dx 1',
            'op_top_dx_2' => 'Op Top Dx 2',
            'op_top_dx_3' => 'Op Top Dx 3',
            'op_top_dx_4' => 'Op Top Dx 4',
            'op_top_dx_5' => 'Op Top Dx 5',
            'd_update' => 'D Update',
        ];
    }
}
