<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "summary_report".
 *
 * @property int $yearbudget
 * @property int|null $op_visit
 * @property int|null $op_hn
 * @property float|null $op_per_day
 * @property int|null $op_refer
 * @property int|null $op_death
 * @property int|null $op_accdent
 * @property int|null $op_head_injury
 * @property int|null $op_stroke
 * @property int|null $op_stemi
 * @property int|null $ip_admit
 * @property int|null $ip_refer
 * @property int|null $ip_losd
 * @property float|null $ip_bed_rate
 * @property float|null $ip_sum_adjrw
 * @property float|null $ip_cmi
 * @property int|null $op_uc
 * @property int|null $op_sss
 * @property int|null $op_lgo
 * @property int|null $op_ofc
 * @property int|null $op_other
 * @property int|null $ip_uc
 * @property int|null $ip_sss
 * @property int|null $ip_lgo
 * @property int|null $ip_ofc
 * @property int|null $ip_other
 * @property string|null $d_update
 */
class SummaryReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'summary_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearbudget'], 'required'],
            [['yearbudget', 'op_visit', 'op_hn', 'op_refer', 'op_death', 'op_accdent', 'op_head_injury', 'op_stroke', 'op_stemi', 'ip_admit', 'ip_refer', 'ip_losd', 'op_uc', 'op_sss', 'op_lgo', 'op_ofc', 'op_other', 'ip_uc', 'ip_sss', 'ip_lgo', 'ip_ofc', 'ip_other'], 'integer'],
            [['op_per_day', 'ip_bed_rate', 'ip_sum_adjrw', 'ip_cmi'], 'number'],
            [['d_update'], 'safe'],
            [['yearbudget'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'yearbudget' => 'ปีงบประมาณ',
            'op_visit' => 'จำนวนครั้งผู้มารับบริการ',
            'op_hn' => 'จำนวนคนที่มารับบริการ',
            'op_per_day' => 'อัตราผู้มารับบริการต่อวัน',
            'op_refer' => 'จำนวนการส่งต่อผู้ป่วยนอก',
            'op_death' => 'จำนวนเคสตายผู้ป่วยนอก',
            'op_accdent' => 'จำนวนเคส พรบ.',
            'op_head_injury' => 'จำนวนเคส HI',
            'op_stroke' => 'จำนวนเคส Stroke',
            'op_stemi' => 'จำนวนเคส Stemi',
            'ip_admit' => 'จำนวนรับรักษาผู้ป่วยใน',
            'ip_refer' => 'จำนวนส่งต่อผู้ป่วยใน',
            'ip_losd' => 'จำนวนวันนอน',
            'ip_bed_rate' => 'อัตราครองเตียง',
            'ip_sum_adjrw' => 'Sum Adjrw',
            'ip_cmi' => 'CMI',
            'op_uc' => 'OP UC',
            'op_sss' => 'OP ประกันสังคม',
            'op_lgo' => 'OP พนักงานท้องถิ่น',
            'op_ofc' => 'OP ข้าราชการ',
            'op_other' => 'OP อื่นๆ',
            'ip_uc' => 'IP UC',
            'ip_sss' => 'IP ประกันสังคม',
            'ip_lgo' => 'IP ท้องถิ่น',
            'ip_ofc' => 'IP ข้าราชการ',
            'ip_other' => 'IP อื่นๆ',
            'd_update' => 'วันที่ปรับปรุงข้อมูล',
        ];
    }
}
