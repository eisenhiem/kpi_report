<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FeeTemplate;

/**
 * FeeTemplateSearch represents the model behind the search form of `app\models\FeeTemplate`.
 */
class FeeTemplateSearch extends FeeTemplate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fee_schedule_id'], 'integer'],
            [['fee_schedule_name', 'template'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FeeTemplate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'fee_schedule_id' => $this->fee_schedule_id,
        ]);

        $query->andFilterWhere(['like', 'fee_schedule_name', $this->fee_schedule_name])
            ->andFilterWhere(['like', 'template', $this->template]);

        return $dataProvider;
    }
}
