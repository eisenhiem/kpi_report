<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FeeSchedule;

/**
 * FeeScheduleSearch represents the model behind the search form of `app\models\FeeSchedule`.
 */
class FeeScheduleSearch extends FeeSchedule
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'fee_schedule_id', 'target', 'result', 'rep', 'deny'], 'integer'],
            [['report_date', 'd_update'], 'safe'],
            [['result_total', 'rep_total', 'rec'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FeeSchedule::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'report_date' => $this->report_date,
            'fee_schedule_id' => $this->fee_schedule_id,
            'target' => $this->target,
            'result' => $this->result,
            'result_total' => $this->result_total,
            'rep' => $this->rep,
            'deny' => $this->deny,
            'rep_total' => $this->rep_total,
            'rec' => $this->rec,
            'd_update' => $this->d_update,
        ]);

        return $dataProvider;
    }
}
