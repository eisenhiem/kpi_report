<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SummaryReport;

/**
 * SummaryReportSearch represents the model behind the search form of `app\models\SummaryReport`.
 */
class SummaryReportSearch extends SummaryReport
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearbudget', 'op_visit', 'op_hn', 'op_refer', 'op_death', 'op_accdent', 'op_head_injury', 'op_stroke', 'op_stemi', 'ip_admit', 'ip_refer', 'ip_losd', 'op_uc', 'op_sss', 'op_lgo', 'op_ofc', 'op_other', 'ip_uc', 'ip_sss', 'ip_lgo', 'ip_ofc', 'ip_other'], 'integer'],
            [['op_per_day', 'ip_bed_rate', 'ip_sum_adjrw', 'ip_cmi'], 'number'],
            [['d_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SummaryReport::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'yearbudget' => $this->yearbudget,
            'op_visit' => $this->op_visit,
            'op_hn' => $this->op_hn,
            'op_per_day' => $this->op_per_day,
            'op_refer' => $this->op_refer,
            'op_death' => $this->op_death,
            'op_accdent' => $this->op_accdent,
            'op_head_injury' => $this->op_head_injury,
            'op_stroke' => $this->op_stroke,
            'op_stemi' => $this->op_stemi,
            'ip_admit' => $this->ip_admit,
            'ip_refer' => $this->ip_refer,
            'ip_losd' => $this->ip_losd,
            'ip_bed_rate' => $this->ip_bed_rate,
            'ip_sum_adjrw' => $this->ip_sum_adjrw,
            'ip_cmi' => $this->ip_cmi,
            'op_uc' => $this->op_uc,
            'op_sss' => $this->op_sss,
            'op_lgo' => $this->op_lgo,
            'op_ofc' => $this->op_ofc,
            'op_other' => $this->op_other,
            'ip_uc' => $this->ip_uc,
            'ip_sss' => $this->ip_sss,
            'ip_lgo' => $this->ip_lgo,
            'ip_ofc' => $this->ip_ofc,
            'ip_other' => $this->ip_other,
            'd_update' => $this->d_update,
        ]);

        return $dataProvider;
    }
}
