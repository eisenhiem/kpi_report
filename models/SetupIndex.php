<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\AttributeBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "setup_index".
 *
 * @property int $id
 * @property string|null $graph1
 * @property string|null $graph2
 * @property string|null $graph3
 * @property string|null $graph4
 * @property string $columns_show
 * @property string|null $card1
 * @property string|null $card2
 * @property string|null $card3
 * @property string|null $card4
 */
class SetupIndex extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'setup_index';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'columns_show',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'columns_show',
                ],
                'value' => function ($event) {
                    return implode(',', $this->columns);
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['graph1', 'graph2', 'graph3', 'graph4'], 'string'],
            [['columns_show', 'card1', 'card2', 'card3', 'card4'], 'string', 'max' => 255],
            [['columns_show'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'graph1' => 'Graph1',
            'graph2' => 'Graph2',
            'graph3' => 'Graph3',
            'graph4' => 'Graph4',
            'columns_show' => 'Columns Show',
            'card1' => 'Card1',
            'card2' => 'Card2',
            'card3' => 'Card3',
            'card4' => 'Card4',
        ];
    }

    public function getItems(){
        $items = [
            'yearbudget' => 'ปีงบประมาณ',
            'op_visit' => 'จำนวนครั้งผู้มารับบริการ',
            'op_hn' => 'จำนวนคนที่มารับบริการ',
            'op_per_day' => 'อัตราผู้มารับบริการต่อวัน',
            'op_refer' => 'จำนวนการส่งต่อผู้ป่วยนอก',
            'op_death' => 'จำนวนเคสตายผู้ป่วยนอก',
            'op_accdent' => 'จำนวนเคส พรบ.',
            'op_head_injury' => 'จำนวนเคส HI',
            'op_stroke' => 'จำนวนเคส Stroke',
            'op_stemi' => 'จำนวนเคส Stemi',
            'ip_admit' => 'จำนวนรับรักษาผู้ป่วยใน',
            'ip_refer' => 'จำนวนส่งต่อผู้ป่วยใน',
            'ip_losd' => 'จำนวนวันนอน',
            'ip_bed_rate' => 'อัตราครองเตียง',
            'ip_sum_adjrw' => 'Sum Adjrw',
            'ip_cmi' => 'CMI',
            'op_uc' => 'OP UC',
            'op_sss' => 'OP ประกันสังคม',
            'op_lgo' => 'OP พนักงานท้องถิ่น',
            'op_ofc' => 'OP ข้าราชการ',
            'op_other' => 'OP อื่นๆ',
            'ip_uc' => 'IP UC',
            'ip_sss' => 'IP ประกันสังคม',
            'ip_lgo' => 'IP ท้องถิ่น',
            'ip_ofc' => 'IP ข้าราชการ',
            'ip_other' => 'IP อื่นๆ',    
        ];
        return $items;
    }

    public function columnToArray()
    {
      return $this->columns_show = explode(',', $this->columns_show);
    }
}
