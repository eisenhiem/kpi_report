<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ip_top_dx".
 *
 * @property int $yearbudget
 * @property string|null $ip_top_dx_1
 * @property string|null $ip_top_dx_2
 * @property string|null $ip_top_dx_3
 * @property string|null $ip_top_dx_4
 * @property string|null $ip_top_dx_5
 * @property string|null $d_update
 */
class IpTopDx extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ip_top_dx';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearbudget'], 'required'],
            [['yearbudget'], 'integer'],
            [['d_update'], 'safe'],
            [['ip_top_dx_1', 'ip_top_dx_2', 'ip_top_dx_3', 'ip_top_dx_4', 'ip_top_dx_5'], 'string', 'max' => 255],
            [['yearbudget'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'yearbudget' => 'Yearbudget',
            'ip_top_dx_1' => 'Ip Top Dx 1',
            'ip_top_dx_2' => 'Ip Top Dx 2',
            'ip_top_dx_3' => 'Ip Top Dx 3',
            'ip_top_dx_4' => 'Ip Top Dx 4',
            'ip_top_dx_5' => 'Ip Top Dx 5',
            'd_update' => 'D Update',
        ];
    }
}
