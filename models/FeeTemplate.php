<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fee_template".
 *
 * @property int $fee_schedule_id
 * @property string|null $fee_schedule_name
 * @property string|null $template
 */
class FeeTemplate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fee_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['template'], 'string'],
            [['fee_schedule_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fee_schedule_id' => 'Fee Schedule ID',
            'fee_schedule_name' => 'รายการ Fee Schedule',
            'template' => 'Template',
        ];
    }
}
