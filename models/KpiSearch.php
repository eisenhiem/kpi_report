<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kpi;

/**
 * KpiSearch represents the model behind the search form of `app\models\Kpi`.
 */
class KpiSearch extends Kpi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kpi_id'], 'integer'],
            [['kpi_name', 'kpi_template', 'kpi_operate', 'kpi_owner', 'kpi_from'], 'safe'],
            [['kpi_target'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kpi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kpi_id' => $this->kpi_id,
            'kpi_target' => $this->kpi_target,
        ]);

        $query->andFilterWhere(['like', 'kpi_name', $this->kpi_name])
            ->andFilterWhere(['like', 'kpi_template', $this->kpi_template])
            ->andFilterWhere(['like', 'kpi_operate', $this->kpi_operate])
            ->andFilterWhere(['like', 'kpi_owner', $this->kpi_owner])
            ->andFilterWhere(['like', 'kpi_from', $this->kpi_from]);

        return $dataProvider;
    }
}
