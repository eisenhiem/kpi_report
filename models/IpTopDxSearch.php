<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IpTopDx;

/**
 * IpTopDxSearch represents the model behind the search form of `app\models\IpTopDx`.
 */
class IpTopDxSearch extends IpTopDx
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearbudget'], 'integer'],
            [['ip_top_dx_1', 'ip_top_dx_2', 'ip_top_dx_3', 'ip_top_dx_4', 'ip_top_dx_5', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IpTopDx::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'yearbudget' => $this->yearbudget,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'ip_top_dx_1', $this->ip_top_dx_1])
            ->andFilterWhere(['like', 'ip_top_dx_2', $this->ip_top_dx_2])
            ->andFilterWhere(['like', 'ip_top_dx_3', $this->ip_top_dx_3])
            ->andFilterWhere(['like', 'ip_top_dx_4', $this->ip_top_dx_4])
            ->andFilterWhere(['like', 'ip_top_dx_5', $this->ip_top_dx_5]);

        return $dataProvider;
    }
}
