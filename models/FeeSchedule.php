<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fee_schedule".
 *
 * @property int $id
 * @property string|null $report_date
 * @property int|null $fee_schedule_id
 * @property int|null $target
 * @property int|null $result
 * @property float|null $result_total
 * @property int|null $rep
 * @property string|null $rep_no
 * @property float|null $rep_total
 * @property float|null $rec
 * @property string|null $d_update
 */
class FeeSchedule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fee_schedule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['report_date', 'd_update'], 'safe'],
            [['fee_schedule_id', 'target', 'result', 'rep','deny'], 'integer'],
            [['result_total', 'rep_total', 'rec'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_date' => 'วันที่รายงาน',
            'fee_schedule_id' => 'Fee Schedule ID',
            'target' => 'เป้าหมาย',
            'result' => 'ผลงาน',
            'result_total' => 'ประมาณการรายรับ',
            'rep' => 'ผลงานตอบกลับ',
            'deny' => 'ปฏิเสธการจ่าย',
            'rep_total' => 'เคลมได้',
            'rec' => 'ยอดโอนมา',
            'd_update' => 'D Update',
        ];
    }

    public function getFeetemplate(){
        return $this->hasOne(FeeTemplate::className(),['fee_schedule_id' => 'fee_schedule_id']);
    }

    public function getFeeName(){
        return $this->feetemplate->fee_schedule_name;
    }

}
