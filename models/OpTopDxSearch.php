<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OpTopDx;

/**
 * OpTopDxSearch represents the model behind the search form of `app\models\OpTopDx`.
 */
class OpTopDxSearch extends OpTopDx
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearbudget'], 'integer'],
            [['op_top_dx_1', 'op_top_dx_2', 'op_top_dx_3', 'op_top_dx_4', 'op_top_dx_5', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OpTopDx::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'yearbudget' => $this->yearbudget,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'op_top_dx_1', $this->op_top_dx_1])
            ->andFilterWhere(['like', 'op_top_dx_2', $this->op_top_dx_2])
            ->andFilterWhere(['like', 'op_top_dx_3', $this->op_top_dx_3])
            ->andFilterWhere(['like', 'op_top_dx_4', $this->op_top_dx_4])
            ->andFilterWhere(['like', 'op_top_dx_5', $this->op_top_dx_5]);

        return $dataProvider;
    }
}
