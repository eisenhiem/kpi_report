<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "refer_top_dx".
 *
 * @property int $yearbudget
 * @property string|null $rf_top_dx_1
 * @property string|null $rf_top_dx_2
 * @property string|null $rf_top_dx_3
 * @property string|null $rf_top_dx_4
 * @property string|null $rf_top_dx_5
 * @property string|null $d_update
 */
class ReferTopDx extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'refer_top_dx';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['yearbudget'], 'required'],
            [['yearbudget'], 'integer'],
            [['d_update'], 'safe'],
            [['rf_top_dx_1', 'rf_top_dx_2', 'rf_top_dx_3', 'rf_top_dx_4', 'rf_top_dx_5'], 'string', 'max' => 255],
            [['yearbudget'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'yearbudget' => 'Yearbudget',
            'rf_top_dx_1' => 'Rf Top Dx 1',
            'rf_top_dx_2' => 'Rf Top Dx 2',
            'rf_top_dx_3' => 'Rf Top Dx 3',
            'rf_top_dx_4' => 'Rf Top Dx 4',
            'rf_top_dx_5' => 'Rf Top Dx 5',
            'd_update' => 'D Update',
        ];
    }
}
